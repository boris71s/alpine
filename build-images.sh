#!/bin/bash

# list of all tags to build,
tags=(
  3.14
  3.15
  3.16
  3.17
)
repository=boris71s/alpine

# build all defined tags
for tag in ${tags[*]}; do
  printf "building %s\n" "${repository}":"${tag}"
  docker build -t "${repository}":"${tag}" --build-arg BASE_TAG="${tags}" .
  printf "\n%s build complete\n\n\n" "${repository}":"${tag}"
done
# tag latest built as latest
docker build -t "${repository}":latest --build-arg BASE_TAG="${tags[-1]}" .

# push all tags
for tag in ${tags[*]}; do
  printf "pushing %s\n" "${repository}":"${tag}"
  docker push "${repository}":"${tag}"
  printf "\n%s push complete\n\n\n" "${repository}":"${tag}"
done
docker push "${repository}":latest