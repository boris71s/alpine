ARG BASE_TAG=fpm:latest
FROM alpine:${BASE_TAG}

# updating and install required packages
RUN apk update && \
    apk add --no-cache ca-certificates git wget -U tzdata