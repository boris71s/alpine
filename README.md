# Alpine

**A docker container based on `alpine` which comes with `wget` and `git` installed.**

## Usage

To start a shell session in a new alpine container enter the following command.

```shell
docker run -it boris71s/alpine /bin/sh
```

Please keep in mind that there is no `ENTRYPOINT` defined. For more details visit the official [alpine docker hub](https://hub.docker.com/_/alpine).

---
Check out the [docker repository](https://hub.docker.com/r/boris71s/alpine) for a list of versions.